﻿using FirstMigration1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMigration1
{
    class ApplicationDbContext : DbContext
    {
        DbSet<Customer> Customers { get; set; }
        DbSet<Post> Posts { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(@"C:\Users\HUAWEI\source\GitRepository\migration\FirstMigration1\FirstMigration1")
                .AddJsonFile("appsettings.json")
                .Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        { 
                modelBuilder.Entity<Customer>()
                .HasIndex(x => x.Login)
                .IsUnique();
        }
    }
}
