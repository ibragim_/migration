﻿using FirstMigration1.Inteerfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FirstMigration1.Models
{
    class Customer : IEntity<int>
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100)] 
        public string Login { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }
    }
}
