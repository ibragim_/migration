﻿using FirstMigration1.Inteerfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMigration1.Models
{
    class Post : IEntity<int>
    {
        public int ID { get; set; }
        public int CustomerId { get; set; }
        public string Comments { get; set; }

    }
}
